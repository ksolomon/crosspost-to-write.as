<?php
/*
Plugin Name: Crosspost to Write.as
Plugin URI:  https://bitbucket.org/ksolomon/crosspost-to-write.as/
Description: Crosspost any new post to write.as
Version:     1.0.0
Author:      Keith Solomon
Author URI:  https://keithsolomon.net/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No direct access');

// Plugin settings page for username and password (and potentially more in the future)
include(dirname(__FILE__).'/settings.php');

// User authentication.  Returns authentication token for use in main function.
function auth_user($user, $pass) {
	// Set up our array to send to API
	$data = array();

	$data['alias'] = $user;
	$data['pass'] = $pass;

	// Encode the data to be sent
	$json_data = json_encode($data);

	// Initiate the API call
	$url = curl_init('https://write.as/api/auth/login');
	curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($url, CURLOPT_POSTFIELDS, $json_data);
	curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($url, CURLOPT_HTTPHEADER, array(
		'Content-Type: application/json',
		'Content-Length: ' . strlen($json_data))
	);

	// API results for our login request.
	$result = curl_exec($url);
	$result = json_decode($result, true);

	// Store the access token so we can pass it to our post function
	$token = $result['data']['access_token'];

	return $token;
}

// The meat of the plugin.  Take the post ID and contents, and ship it off to Write.as, with optional user authentication.
function post_to_writeas($post_ID, $post) {
	$writeas_opts = get_option('crosspost_to_write_as_options_option_name');

	$user = $writeas_opts['username_0'];
	$pass = $writeas_opts['password_1'];

	// Are both authentiacation variables set?  If so, authenticate the user and get our access token.
	if ($user && $pass)
		$token = auth_user($user, $pass);

	// Set up our post data array
	$data = array();

	// Store the title into the array
	$data['title'] = get_the_title($post_ID);

	// Get the content and save it into the array
	$data['body'] = apply_filters('the_content', get_post_field('post_content', $post_ID));

	add_post_meta($post_ID, 'writeas-post-data', implode(',',$data), true);

	// Encode the data to be sent
	$json_data = json_encode($data);

	// cURL headers, with conditional for authentication token
	if ($token)
		$authhead = 'Authorization: Token '.$token;

	$curlheads = [
		'Content-Type: application/json',
		'Content-Length: '.strlen($json_data),
		$authhead
	];

	add_post_meta($post_ID, 'writeas-post-headers', implode(',',$curlheads), true);

	// Initiate the API call
	$url = curl_init('https://write.as/api/posts');
	curl_setopt($url, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($url, CURLOPT_POSTFIELDS, $json_data);
	curl_setopt($url, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($url, CURLOPT_HTTPHEADER, $curlheads);

	// The results of our request, to use later if we want.
	$result = curl_exec($url);
	$result = json_decode($result, true);

	add_post_meta($post_ID, 'writeas-post-response', json_encode($result), true);

	// Extract post ID & token and store it as a custom field on the post
	$pid = $result['data']['id'];
	$ptoken = $result['data']['token'];

	add_post_meta($post_ID, 'writeas-post-id', $pid, true);
	add_post_meta($post_ID, 'writeas-post-token', $ptoken, true);
}

add_action('publish_post', 'post_to_writeas',10,2);
?>