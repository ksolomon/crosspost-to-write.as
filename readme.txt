=== Crosspost to Write.as ===
Contributors: zarathos
Donate link: https://www.paypal.me/ksol075
Tags: crosspost, write.as
Requires at least: 5.1
Tested up to: 5.4
Requires PHP: 7.2
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
 
Crosspost new posts to write.as.
 
== Description ==
 
Crosspost all new posts to [write.as](https://write.as/), either anonymously or as a specific user.

== Frequently Asked Questions ==
 
= Why do this? =
 
Why not?  It's more of an exercise for myself, but if you like it, great!  If not, sorry to have wasted your time.

= How do I post as a specific user? =

Enter your Write.as username and password in the settings page.  If the options are left blank, the default is to post anonymously.

== Installation ==
 
1. Same as any other plugin, either install via the admin panel, or unzip and upload the plugin to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Upgrade Notice ==

Nothing special needed.  Upgrade as would would any other WordPress plugin.

== Screenshots ==

To come.

== Changelog ==
 
= 1.0 =
* Inital version.
 
== Things to Come ==

* Update existing posts on Write.as
* Suggestions welcome